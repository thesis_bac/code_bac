import pybullet as pb
import pybullet_data
import numpy as np
import time
import matplotlib.pyplot as plt

# connect to the pybullet physics server
# with a GUI simulation instead of a background simulation
pb.connect(pb.GUI)
pb.setAdditionalSearchPath(pybullet_data.getDataPath())

# clear and reset the environment, just in case
pb.resetSimulation()
pb.setGravity(0, 0, -9.8)

# step through for the simulation to work for 100000 cycles
# for _ in range(100000):
#    pb.stepSimulation()
# pb.setRealTimeSimulation(1)

# load the floor model from the data directory,
# uses relative path
plane = pb.loadURDF("plane100.urdf")
# cuboid = pb.loadURDF("cuboid.urdf",
#                     basePosition=[0.7, 0.2, 0.3])
# kuka = pb.loadURDF("data/kuka_iiwa/kuka_3f_daffy.urdf",
#                   useFixedBase=1,
#                   basePosition=[1, 1, 0],
#                   baseOrientation=[0, 0, 0, 3])
orientation = pb.getQuaternionFromEuler([0, 0, np.pi])
arm = pb.loadURDF("data/arm.urdf",
                  useFixedBase=1,
                  basePosition=[0, 0, 0.25],
                  baseOrientation=orientation
                  )

for i in range(100):
    pb.stepSimulation()

pb.setJointMotorControl2(arm, 1, pb.POSITION_CONTROL, 3.1415)
pb.setJointMotorControl2(arm, 0, pb.VELOCITY_CONTROL, force=0)
pb.setJointMotorControl2(arm, 1, pb.VELOCITY_CONTROL, force=0)
for _ in range(100000):
   pb.stepSimulation()

#pb.disconnect()

# -----plot practise------
# joint_pos_data = []

# for i in np.arange(0, 10, 0.1):
#     joint_pos = pb.getJointState(arm,0)
#     joint_pos_data.append(joint_pos)

# plt.plot(np.arange(0,10,0.1), joint_pos_data)


# signal generation
# signal_length = np.arange(0, 20, 0.01)
# p1, p2, p3 = 17, 36, 45
# signal_1 = np.sin(2*np.pi*signal_length/p1) + \
#     np.sin(2*np.pi*signal_length/p2) + \
#     np.sin(2*np.pi*signal_length/p3)

# plt.plot(signal_length, signal_1)
# plt.show()

# for i in range(10):
#     pb.applyExternalTorque(arm, 0, [0,0,0.5], flags=pb.LINK_FRAME)
#     time.sleep(0.001)
