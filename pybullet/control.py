import numpy as np
import pybullet as pb

class PDController(object):

  def __init__(self, kis, alphas, stepSize):
    self.kis = kis
    self.alphas = alphas
    self.stepSize = stepSize

  def computePD(self, bodyUniqueId, jointIndices, desiredPositions, kis , alphas,
                maxForces, stepSize, T):
    numJoints = pb.getNumJoints(bodyUniqueId)
    jointStates = pb.getJointStates(bodyUniqueId, jointIndices)

    T1, T2, T3 = T

    q1 = []
    qdot1 = []
    for i in range(numJoints):
      q1.append(jointStates[i][0])
      qdot1.append(jointStates[i][1])

    q = np.array(q1)
    qdot = np.array(qdot1)
    qdes = np.array(desiredPositions)
    qdesOld = qdes.copy()

    # qdotdes = np.array(desiredVelocities)
    qdotdes = np.array([ i / stepSize for i in (qdes - qdesOld)])


    qError = qdes - q
    qdotError = qdotdes - qdot

    Ki = np.diagflat(kis)
    Alpha = np.diagflat(alphas)

    r = qdotError + Alpha.dot(qError)
    r1, r2 = r

    # w estimate block
    # sat function
    def test():
        print(1)

    def sat(beta, wEstold, T):
        pass

    # second term phi function (hardcoded for each period)
    phi1 = np.tanh(t+T1) #??
    phi2 = np.tanh(t+T3) #??
    phi3 = np.tanh(t+T2) #??

    wEstHist = []
    wEst1 = ( + KI*T1*phi1*r1)

    tau = Ki.dot(qError) + Kd.dot(qdotError)
    maxF = np.array(maxForces)
    forces = np.clip(forces, -maxF, maxF)

    qdot1old = qdot1.copy()
    return forces
