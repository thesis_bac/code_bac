import pybullet as pb
import matplotlib.pyplot as plt
import numpy as np

# --- class definitions

class MPControl(kis, alphas, stepSize):

   def __init__(self):
       pass

   def computePD(self, bodyUniqueId, jointIndices, desiredPositions,
                 maxForces, stepSize, T):
       numJoints = pb.getNumJoints(bodyUniqueId)
       jointStates = pb.getJointStates(bodyUniqueId, jointIndices)
       T1, T2, T3 = T


timeStep = 1/1000
pb.connect(pb.GUI)
pb.setTimeStep(timeStep)

# Getting pybullet ready ------
# pybullet robot getting ready with initial states
orientation = pb.getQuaternionFromEuler([0, 0, np.pi])
arm = pb.loadURDF("data/arm.urdf",
                  useFixedBase=1,
                  basePosition=[0, 0, 0.25],
                  baseOrientation=orientation
                  )

# Getting pybullet ready ------

# input signals as sum of periodic signals -----
t_input = np.arange(0, 10, timeStep)
wave = np.sin(2*np.pi*t_input/2) + np.sin(2*np.pi*t_input/3)
wave_cos = np.cos(2*np.pi*t_input/2) + np.cos(2*np.pi*t_input/3)
# wave = np.sin(2*np.pi*t_input)
# wave_cos = np.cos(2*np.pi*t_input)
# wave = np.sin(t_input)
# wave_cos = np.cos(t_input)

# plot array of the t_input and the sin wave
plot = [t_input, wave]

# some values initialized for history plot
desPosOld = 0 # desired position (t-1)
desPosdotHist = [] # desired velocity history
joint1Hist = [] # robot joint position history
joint2Hist = []
t = 0 # time for sat function \in [0,T_i]
tHist = [] # time elapsed history (for sat later)

for desPos in wave:
    print(desPos)
    desPosdot = (desPos - desPosOld) / timeStep
    desPosdotHist.append(desPosdot)

    if t > 2:
        t = 0

    def phifunc(t,T):
        phi = (t**2/T**2)
        return phi
    phi1 = phifunc(t,2)

    joint1 = pb.getJointState(arm, 0)[0]
    joint1Hist.append(joint1)
    joint2 = pb.getJointState(arm, 1)[0]
    joint2Hist.append(joint2)

    pb.stepSimulation()
    desPosOld = desPos
    t += timeStep
    tHist.append(phi1)

# plt.plot(t_input,add, label = "added", color="r")
plt.plot(t_input, tHist, label="time elapsed", color="grey")
# plt.plot(t_input, wave, label="position", color="grey")
# plt.plot(t_input, desPosdotHist, label="velocity", color="green")
# plt.plot(t_input, joint1Hist, label="velocity", color="green")
plt.legend()
plt.show()
pb.disconnect()
