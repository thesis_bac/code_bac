import pybullet as pb
import pybullet_data
import numpy as np

pb.connect(pb.GUI)
pb.setAdditionalSearchPath(pybullet_data.getDataPath())
pb.setRealTimeSimulation(0)

# clear and reset the environment, just in case
pb.resetSimulation()
# pb.setGravity(0, 0, -9.8)


plane = pb.loadURDF("plane100.urdf")


orientation = pb.getQuaternionFromEuler([0, 0, np.pi])
arm = pb.loadURDF("data/arm.urdf",
                  useFixedBase=1,
                  basePosition=[0, 0, 0.25],
                  baseOrientation=orientation
                  )


timeStep = 1/300
pb.setTimeStep(timeStep)
fr = 0.1
while(1):
    pb.setJointMotorControl2(arm, 0, pb.POSITION_CONTROL, 0)
    pb.setJointMotorControl2(arm, 0, pb.VELOCITY_CONTROL, force=0)
    pb.setJointMotorControl2(arm, 1, pb.VELOCITY_CONTROL, force=0)
    pb.setJointMotorControl2(arm, 0, pb.TORQUE_CONTROL, force=fr)
    pb.stepSimulation()
    fr = 0
