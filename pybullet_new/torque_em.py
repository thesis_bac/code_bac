import time
import pybullet as pb
import pybullet_data
import numpy as np

pb.connect(pb.GUI)
pb.setRealTimeSimulation(0)
pb.setAdditionalSearchPath(pybullet_data.getDataPath())

pb.setGravity(0.0, 0.0, 0.0)


#plane = pb.loadURDF("plane100.urdf")
orientation = pb.getQuaternionFromEuler([0, 0, np.pi])
arm = pb.loadURDF("data/arm.urdf",
                  useFixedBase=1,
                  basePosition=[0, 0, 0.25],
                  baseOrientation=orientation
                  )

n_joints = 2

timeStep = 1 / 1000
pb.setTimeStep(timeStep)


#WARMUP
print("**WARMUP**")
for i in range(60000):
    pb.stepSimulation()

print("*END*")

i = 0
forces = [0.001, 0.001] #np.random.rand(n_joints) * 100
while(1):
    i += 1
    if i % (300*1000) == 0:
        #forces = np.random.rand(n_joints) * 100
        print(forces)
    for j, v in enumerate(forces):
        pb.setJointMotorControl2(arm, j, pb.VELOCITY_CONTROL, force=0)
        pb.setJointMotorControl2(arm, j, pb.TORQUE_CONTROL, force=v)

    pb.stepSimulation()

    time.sleep(timeStep)
    if i % int(1.0 / timeStep) == 0:
        print(i/int(1.0 / timeStep))
         # Get joint state
        for j in range(1):
            (jointPositions, jointVelocity, reactionForce,
                bulletMotorTorques) = pb.getJointState(arm, j)
            print(jointPositions, jointVelocity, reactionForce, bulletMotorTorques)
