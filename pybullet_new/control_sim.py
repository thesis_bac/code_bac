import pybullet as pb
import pybullet_data
import numpy as np
import matplotlib.pyplot as plt
import pyplot_themes as themes

# --- definitions for the control ---

# a continous function t in R to phi in [0, time period]


def phifunc(t, T):
    phi = (t**2 / T**2)
    return phi


# saturation function for the learning term in control
# input: control estimate, Max of the periodic signal output
def satfunc(u, beta):
    if u > beta:
        sat = beta
    else:
        sat = u
    return sat


# class MPControl(kis, alphas, stepSize):

#    def __init__(self):
#        pass

#    def computePD(self, bodyUniqueId, jointIndices, desiredPositions,
#                  maxForces, stepSize, T):
#        numJoints = pb.getNumJoints(bodyUniqueId)
#        jointStates = pb.getJointStates(bodyUniqueId, jointIndices)
#        T1, T2, T3 = T

# Getting pybullet ready ------
timeStep = 1 / 10000
n_joints = 2
pb.connect(pb.GUI)
pb.setRealTimeSimulation(0)
pb.setTimeStep(timeStep)
pb.setGravity(0.0, 0.0, 0.0)

# pybullet robot getting ready
pb.setAdditionalSearchPath(pybullet_data.getDataPath())
robot_orient = pb.getQuaternionFromEuler([0, 0, np.pi])
robot_pos = [0, 0, 0.25]
arm = pb.loadURDF("data/arm.urdf",
                  useFixedBase=1,
                  basePosition=robot_pos,
                  baseOrientation=robot_orient
                  )
# set camera to robot
pb.resetDebugVisualizerCamera(1.5, 30, -30, robot_pos)

# WARMUP
print("**WARMUP**")
for i in range(60000):
    pb.stepSimulation()

print("*END*")

# input signal as sum of periodic signals -----
signal_len = 100
t_input = np.arange(0, signal_len, timeStep)
T_1, T_2, T_3 = 2, 3, 4
# wave = np.sin(2*np.pi*t_input/T_1) + np.sin(2*np.pi*t_input/T_2)
wave = np.cos(2*np.pi*t_input/T_1) + \
    np.cos(2*np.pi*t_input/T_2) + \
    np.cos(2*np.pi*t_input/T_3)
# wave = np.sin(2*np.pi*t_input/2)  # sin with period 2
# wave_cos = np.cos(2*np.pi*t_input)
# wave = np.sin(t_input)
# wave_cos = np.cos(t_input)

# some values initialized for history plot
desPosOld = 0  # desired position (t-1)
desPosDotHist = []  # desired velocity history
joint1PosErrHist = []
joint2PosErrHist = []
r1Hist = []

joint1Hist = []  # robot joint position history
joint2Hist = []

t = 0  # time for phi function \in [0,T_i] updated by stepSimulation
t1 = 0  # t1 and t2 for sat and phi functions
t2 = 0
t3 = 0
tHist = []  # time elapsed history for any other function's plots

forces = [0.001, 0.001]

# control parameters
# alpha1, alpha2 = 20, 10 # gain similar to kD/kP
# k1, k2 = 20, 10 # gain similar to kD
# kI1, kI2 = 20, 10
alpha1, alpha2 = 20, 14  # gain similar to kD/kP
k1, k2 = 40, 12  # gain similar to kD
kI1, kI2 = 5, 2  # gain similar to kI
# control parameters in arrays to put the 1,2
# commands in for loops later
Alphas = [alpha1, alpha2]
Ki = [k1, k2]
KI = [kI1, kI2]

# other values for the control
wEst1r1 = 0  # initializing control estimate
wEst1r1Hist = [wEst1r1]  # initialized history of wEst (for sat function)
wEst1r2 = 0  # initializing control estimate
wEst1r2Hist = [wEst1r2]  # initialized history of wEst (for sat function)

wEst2r1 = 0  # initializing control estimate
wEst2r1Hist = [wEst2r1]  # initialized history of wEst (for sat function)
wEst2r2 = 0  # initializing control estimate
wEst2r2Hist = [wEst2r2]  # initialized history of wEst (for sat function)

wEst3r1 = 0  # initializing control estimate
wEst3r1Hist = [wEst2r1]  # initialized history of wEst (for sat function)
wEst3r2 = 0  # initializing control estimate
wEst3r2Hist = [wEst2r2]  # initialized history of wEst (for sat function)

# --- simulation code starts here ---

for desPos in wave:
    # same desired input positions for both links
    print(desPos)

    # desired velocity (desired position derivative)
    desPosDot = (desPos - desPosOld) / timeStep
    # des vel history for plot
    desPosDotHist.append(desPosDot)

    # get joint angles q1 and q2
    joint1Pos = pb.getJointState(arm, 0)[0]
    joint1Hist.append(joint1Pos)
    joint2Pos = pb.getJointState(arm, 1)[0]
    joint2Hist.append(joint2Pos)

    # get joint velocities q1dot and q2dot
    joint1Vel = pb.getJointState(arm, 0)[0]
    joint2Vel = pb.getJointState(arm, 1)[0]

    # errors for control
    joint1PosErr = desPos - joint1Pos
    joint2PosErr = desPos - joint2Pos
    joint1VelErr = desPosDot - joint1Vel
    joint2VelErr = desPosDot - joint2Vel

    joint1PosErrHist.append(joint1PosErr)
    joint2PosErrHist.append(joint2PosErr)

    # r term in the control
    # r = qDot - q*alpha1
    r1 = joint1VelErr + alpha1*joint1PosErr
    r2 = joint2VelErr + alpha2*joint2PosErr
    r1Hist.append(r1)

    # !for trial mod 2 for phi, define phi better!
    # T_1 = 2 # giving the time period of the signal
    # # there will be many time periods in testing signals
    # T_2 = 3 # giving the time period of the signal
    if t1 > T_1:
        t1 = 0
    phiT_1 = phifunc(t1, T_1)

    if t2 > T_2:
        t2 = 0
    phiT_2 = phifunc(t, T_2)

    if t3 > T_3:
        t3 = 0
    phiT_3 = phifunc(t, T_3)

    # checking to keep wEst=0 t<=0 in history
    if len(wEst1r1Hist) < T_1:
        u1r1 = wEst1r1Hist[0]
    else:
        u1r1 = wEst1r1Hist[-T_1]

    if len(wEst1r2Hist) < T_1:
        u1r2 = wEst1r2Hist[0]
    else:
        u1r2 = wEst1r2Hist[-T_1]

    sat1r1 = satfunc(u1r1, beta=3)
    sat1r2 = satfunc(u1r2, beta=3)

    wEst1r1 = sat1r1 + kI1*T_1*phiT_1*r1
    wEst1r1Hist.append(wEst1r1)
    wEst1r2 = sat1r2 + kI2*T_1*phiT_1*r2
    wEst1r2Hist.append(wEst1r2)

    if len(wEst2r1Hist) < T_2:
        u2r1 = wEst2r1Hist[0]
    else:
        u2r1 = wEst2r1Hist[-T_2]

    if len(wEst2r2Hist) < T_2:
        u2r2 = wEst2r2Hist[0]
    else:
        u2r2 = wEst2r2Hist[-T_2]

    sat2r1 = satfunc(u2r1, beta=3)
    sat2r2 = satfunc(u2r2, beta=3)

    wEst2r1 = sat2r1 + kI1*T_2*phiT_2*r1
    wEst2r1Hist.append(wEst2r1)
    wEst2r2 = sat2r2 + kI2*T_2*phiT_2*r2
    wEst2r2Hist.append(wEst2r2)

    if len(wEst3r1Hist) < T_3:
        u3r1 = wEst3r1Hist[0]
    else:
        u3r1 = wEst3r1Hist[-T_3]

    if len(wEst3r2Hist) < T_3:
        u3r2 = wEst3r2Hist[0]
    else:
        u3r2 = wEst3r2Hist[-T_3]

    sat3r1 = satfunc(u2r1, beta=3)
    sat3r2 = satfunc(u2r2, beta=3)

    wEst3r1 = sat3r1 + kI1*T_3*phiT_3*r1
    wEst3r1Hist.append(wEst2r1)
    wEst3r2 = sat3r2 + kI2*T_2*phiT_3*r2
    wEst3r2Hist.append(wEst3r2)

    wEstr1 = wEst1r1 + wEst2r1 + wEst3r1
    wEstr2 = wEst2r2 + wEst2r2 + wEst3r2

    tau1 = k1*r1 + wEstr1
    tau2 = k2*r2 + wEstr2
    forces[0] = tau1
    forces[1] = tau2

    for j, v in enumerate(forces):
        pb.setJointMotorControl2(arm, j, pb.VELOCITY_CONTROL, force=0)
        pb.setJointMotorControl2(arm, j, pb.TORQUE_CONTROL, force=v)

    pb.stepSimulation()
    desPosOld = desPos
    t1 += timeStep/1000000
    t2 += timeStep/1000000
    t3 += timeStep/1000000

#  --- some plots ---
themes.theme_few()
# plt.plot(t_input, wave, label = "input signal", color="r")
# plt.plot(t_input, joint1PosErrHist, label="joint 1 position error", color="black")
# plt.plot(t_input, joint2PosErrHist, label="j2 pos err", color="b")
plt.plot(t_input, wEst1r1Hist[0:(len(wEst1r1Hist) -1)], label="wEstr1", color="darkblue")
# plt.plot(t_input, wave, label="position", color="grey")
# plt.plot(t_input, desPosdotHist, label="velocity", color="green")
# plt.plot(t_input, joint1Hist, label="joint1 pos", color="green")
# plt.plot(t_input, joint2Hist, label="joint2 pos", color="green")
plt.legend()
plt.show()
pb.disconnect()
