import pybullet as pb
import pybullet_data
import numpy as np

pb.connect(pb.GUI)
pb.setAdditionalSearchPath(pybullet_data.getDataPath())
plane = pb.loadURDF("plane100.urdf")
orientation = pb.getQuaternionFromEuler([0, 0, np.pi])
arm = pb.loadURDF("data/arm.urdf",
                  useFixedBase=1,
                  basePosition=[0, 0, 0.25],
                  baseOrientation=orientation
                  )
pb.setRealTimeSimulation(1)
